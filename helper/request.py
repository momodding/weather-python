import urllib.request
import json

class Request():

    def anotherRequestGet(self, url):
        self.__url = url

        response = urllib.request.urlopen(self.__url)
        data = response.read()
        JSON_object = json.loads(data.decode('utf-8'))

        return JSON_object