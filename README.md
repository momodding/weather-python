Package:
- Python 3.6.8

Penggunaan:

- search by city : python3 main.py -o=city -c=fran

- search by coordinate : python3 main.py -o=coordinate -coord=36.96,-122.02

- save by city : python3 main.py -o=save -c=fran

- show all saved file : python3 main.py  -o=show_all 

- remove city : python3 main.py -o=remove 

Code Coverage:
![Alt text](code-coverage.png?raw=true "Optional Title")