# -*- coding: utf-8 -*-
import controller.weathercontroller as weather
import datetime
import json
import glob
import errno
import os
from argparse import ArgumentParser

if __name__ == '__main__':
    parser = ArgumentParser(description="Weather")
    parser.add_argument('-o', '--option', type=str, default="city", help="search location")
    parser.add_argument('-c', '--city', type=str, default="london", help="search location by query")
    parser.add_argument('-coord', '--coordinate', type=str, default="36.96,-122.02", help="search location by coordinate")

    args = parser.parse_args()

    if args.option == "city":

        if args.city is not None:

            cuaca = weather.Weather()
            result = cuaca.LocationByCity(args.city)

            print("data found:")

            for i in range(0, len(result), 1):
                print(i + 1, '.', result[i]['title'])

            if (len(result) > 1):

                menu = int(input("Which one that you want to see (in number, ex: 1)?"))

                print("weather for city ", result[menu-1]['title'])

                cuaca_seminggu = cuaca.LocationByWoeid(str(result[menu - 1]['woeid']))

                print("Date\t\t:",
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][0]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][1]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][2]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][3]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][4]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][5]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t')

                print("Weather\t\t:",
                      cuaca_seminggu["consolidated_weather"][0]['weather_state_name'], '\t',
                      cuaca_seminggu["consolidated_weather"][1]['weather_state_name'], '\t',
                      cuaca_seminggu["consolidated_weather"][2]['weather_state_name'], '\t',
                      cuaca_seminggu["consolidated_weather"][3]['weather_state_name'], '\t',
                      cuaca_seminggu["consolidated_weather"][4]['weather_state_name'], '\t',
                      cuaca_seminggu["consolidated_weather"][5]['weather_state_name'], '\t')

                print("Min. Temp\t:",
                      int(round(cuaca_seminggu["consolidated_weather"][0]['min_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][1]['min_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][2]['min_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][3]['min_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][4]['min_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][5]['min_temp'], 0)), '\u00b0\t')

                print("Max. Temp\t:",
                      int(round(cuaca_seminggu["consolidated_weather"][0]['max_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][1]['max_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][2]['max_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][3]['max_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][4]['max_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][5]['max_temp'], 0)), '\u00b0\t')

                print("Temp\t\t:",
                      int(round(cuaca_seminggu["consolidated_weather"][0]['the_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][1]['the_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][2]['the_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][3]['the_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][4]['the_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][5]['the_temp'], 0)), '\u00b0\t')

    elif args.option == "coordinate":

        if args.coordinate is not None:

            cuaca = weather.Weather()
            result = cuaca.LocationByCoordinate(args.coordinate)

            print("data found:")

            for i in range(0, len(result), 1):
                print(i + 1, '.', result[i]['title'])

            if (len(result) > 1):

                menu = int(input("Which one that you want to see (in number, ex: 1)?"))

                print("weather for city ", result[menu-1]['title'])

                cuaca_seminggu = cuaca.LocationByWoeid(str(result[menu - 1]['woeid']))

                print("Date\t\t:",
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][0]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][1]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][2]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][3]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][4]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                      datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][5]['created'],
                                                 "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t')

                print("Weather\t\t:",
                      cuaca_seminggu["consolidated_weather"][0]['weather_state_name'], '\t',
                      cuaca_seminggu["consolidated_weather"][1]['weather_state_name'], '\t',
                      cuaca_seminggu["consolidated_weather"][2]['weather_state_name'], '\t',
                      cuaca_seminggu["consolidated_weather"][3]['weather_state_name'], '\t',
                      cuaca_seminggu["consolidated_weather"][4]['weather_state_name'], '\t',
                      cuaca_seminggu["consolidated_weather"][5]['weather_state_name'], '\t')

                print("Min. Temp\t:",
                      int(round(cuaca_seminggu["consolidated_weather"][0]['min_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][1]['min_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][2]['min_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][3]['min_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][4]['min_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][5]['min_temp'], 0)), '\u00b0\t')

                print("Max. Temp\t:",
                      int(round(cuaca_seminggu["consolidated_weather"][0]['max_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][1]['max_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][2]['max_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][3]['max_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][4]['max_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][5]['max_temp'], 0)), '\u00b0\t')

                print("Temp\t\t:",
                      int(round(cuaca_seminggu["consolidated_weather"][0]['the_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][1]['the_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][2]['the_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][3]['the_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][4]['the_temp'], 0)), '\u00b0\t',
                      int(round(cuaca_seminggu["consolidated_weather"][5]['the_temp'], 0)), '\u00b0\t')

    elif args.option == 'save':

        if args.city is not None:

            location = "storage"

            cuaca = weather.Weather()
            result = cuaca.LocationByCity(args.city)

            print("data found:")

            for i in range(0, len(result), 1):
                print(i + 1, '.', result[i]['title'])

            if (len(result) > 1):
                menu = int(input("Which one that you want to see (in number, ex: 1)?"))

                print("City with name ", result[menu - 1]['title'], " has been saved")

                cuaca_seminggu = cuaca.LocationByWoeid(str(result[menu - 1]['woeid']))

                with open(location + "/" + result[menu - 1]['title'] + '.txt', 'w') as outfile:
                    json.dump(cuaca_seminggu, outfile)

    elif args.option == 'show_all':

        path = 'storage/*.txt'
        files = glob.glob(path)
        for name in files:
            try:
                with open(name) as f:
                    cuaca_seminggu = json.load(f)

                    print("weather for city ", cuaca_seminggu['title'])

                    print("Date\t\t:",
                          datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][0]['created'],
                                                     "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                          datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][1]['created'],
                                                     "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                          datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][2]['created'],
                                                     "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                          datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][3]['created'],
                                                     "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                          datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][4]['created'],
                                                     "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t',
                          datetime.datetime.strptime(cuaca_seminggu["consolidated_weather"][5]['created'],
                                                     "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%d'), '\t')

                    print("Weather\t\t:",
                          cuaca_seminggu["consolidated_weather"][0]['weather_state_name'], '\t',
                          cuaca_seminggu["consolidated_weather"][1]['weather_state_name'], '\t',
                          cuaca_seminggu["consolidated_weather"][2]['weather_state_name'], '\t',
                          cuaca_seminggu["consolidated_weather"][3]['weather_state_name'], '\t',
                          cuaca_seminggu["consolidated_weather"][4]['weather_state_name'], '\t',
                          cuaca_seminggu["consolidated_weather"][5]['weather_state_name'], '\t')

                    print("Min. Temp\t:",
                          int(round(cuaca_seminggu["consolidated_weather"][0]['min_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][1]['min_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][2]['min_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][3]['min_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][4]['min_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][5]['min_temp'], 0)), '\u00b0\t')

                    print("Max. Temp\t:",
                          int(round(cuaca_seminggu["consolidated_weather"][0]['max_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][1]['max_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][2]['max_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][3]['max_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][4]['max_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][5]['max_temp'], 0)), '\u00b0\t')

                    print("Temp\t\t:",
                          int(round(cuaca_seminggu["consolidated_weather"][0]['the_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][1]['the_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][2]['the_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][3]['the_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][4]['the_temp'], 0)), '\u00b0\t',
                          int(round(cuaca_seminggu["consolidated_weather"][5]['the_temp'], 0)), '\u00b0\t\n')

            except IOError as exc:
                if exc.errno != errno.EISDIR:
                    raise

    elif args.option == 'remove':

        path = 'storage/*.txt'
        files = glob.glob(path)
        for i in range(0, len(files), 1):
            print(i+1, ".", files[i].split('/')[1].split(".")[0])

        menu = int(input("Which one that you want to remove (in number, ex: 1)?"))

        os.remove(files[menu-1])
        print("City with name ", files[menu-1].split('/')[1].split(".")[0], " has been removed")

    else:
        print("Perintah tidak ditemukan")