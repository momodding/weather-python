import helper.request as request

class Weather():
    def LocationByCity(self, loc):
        req = request.Request()
        url = "https://www.metaweather.com/api/location/search/?query="
        result = req.anotherRequestGet(url + loc)

        return result

    def LocationByCoordinate(self, loc):
        req = request.Request()
        url = "https://www.metaweather.com/api/location/search/?lattlong="
        result = req.anotherRequestGet(url + loc)

        return result

    def LocationByWoeid(self, loc):
        req = request.Request()
        url = "https://www.metaweather.com/api/location/"
        result = req.anotherRequestGet(url + loc)

        return result

# if __name__ == '__main__':
#     cuaca = Weather()
#     hasil = cuaca.LocationByWoeid("44418")
#
#     print(hasil)