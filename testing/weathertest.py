import unittest
from controller import weathercontroller

class WeatherTest(unittest.TestCase):

    def test_LocationByCity(self):
        cuaca = weathercontroller.Weather()

        result = cuaca.LocationByCity("London")

        self.assertEqual("London", result[0]['title'])

    def test_LocationByCoordinate(self):
        cuaca = weathercontroller.Weather()

        result = cuaca.LocationByCoordinate("36.96,-122.02")

        self.assertEqual("Santa Cruz", result[0]['title'])

    def test_LocationByWoeid(self):
        cuaca = weathercontroller.Weather()

        result = cuaca.LocationByWoeid("44418")

        self.assertEqual("London", result['title'])

if __name__ == '__main__':
    unittest.main()